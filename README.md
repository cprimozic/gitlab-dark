# GitLab Dark Theme

A dark theme for the website [GitLab.com](https://gitlab.com/maxigaz/gitlab-dark).

This style is written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format, specifically for [Stylus](https://add0n.com/stylus.html), although it might be compatible with other style managers such as [xStyle](https://github.com/FirefoxBar/xStyle).

## Install with Stylus

If you have Stylus installed, click on one of the banners below and a new window of Stylus should open, asking you to confirm to install the style.

**Stable version** (updated less frequently, with version number increase): [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/gitlab-dark/raw/stable/gitlab-dark.user.css)  
**Development version** (updated after every commit, reinstall it manually to see the latest changes): [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/gitlab-dark/raw/master/gitlab-dark.user.css)

## Mirrors

The style can also be installed from the following websites (although these versions may be slightly out of date):

[OpenUserCSS](https://openusercss.org/theme/5b314c73ae380a0b00767cfa)  
[Userstyles.org](https://userstyles.org/styles/163451/gitlab-dark-maxigaz)

## Screenshot

![A screenshot showing the main page of a project](Screenshot1.png)

## Submitting issues

Before you open a new issue ticket, please, make sure you’ve done the following:

- Read the known issues listed below.
- Install the latest development version. (Open the style from [here](https://gitlab.com/maxigaz/gitlab-dark/raw/master/gitlab-dark.user.css). If you have installed it earlier, tell Stylus to reinstall it, overriding the version you currently have.)

In the issue description, include the following:

- An example URL to the page you’re experiencing the problem on or provide step by step instructions on how to reproduce it.
- The version of your web browser and the userstyle.
- In addition, including a screenshot or screencast of the problem is also helpful.

## Known issues

### The top and left navigation bars look out of place

The navigation bar on the top of the page may have a different background colour and some items in the left sidebar may appear too dark to be legible. (See the first screenshot in [this issue](https://gitlab.com/maxigaz/gitlab-dark/issues/21) for reference.) The reason for this is that different built-in navigation themes have different CSS selectors, and this userstyle only covers the default theme (Indigo).

So, if this happens, go to User Settings → [Preferences](https://gitlab.com/profile/preferences), and change the Navigation theme to "Indigo".

### The style doesn't work on self-hosted instances at all

The reason is that the domain of each self-hosted instance would need to be added to the style as a `@-moz-document` rule one by one. Since it's impossible to know all these domains, I've added only gitlab.com and framagit.org for now.

If you want the style to be applied to your own instance, open it in Stylus (or whatever style manager you're using) and edit the following line near the top of its code:

```
@-moz-document url-prefix("https://gitlab."), domain("gitlab.com"), domain("framagit.org")
```

Note that you need to do this every time you upgrade to the latest version.

As an alternative, if your instance is publicly available and you don't mind if its domain is visible to others, I can add it in the upstream version. (Please, open an issue for it.)

### Some elements look out of place on self-hosted instances but they look fine on GitLab.com

That's because the layout of GitLab can change between versions. From what I've noticed, such changes land on GitLab.com (running the Enterprise Edition) first—a few days later, a new version of the Community Edition should come out with the same changes. Try upgrading to it.

**GitLab.com is the only instance that is officially supported at the moment.**

## Credits

The userstyle is inspired by [Arc Dark](https://github.com/horst3180/Arc-theme), although it doesn't follow all its design choices.

The logo of the project is based on [the official logo of GitLab](https://about.gitlab.com/press/), licenced under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Licence](https://creativecommons.org/licenses/by-nc-sa/4.0/).

## More userstyles from me

If you like this userstyle, check out the list of all styles created/maintained by me [here](https://gitlab.com/maxigaz/userstyles).
